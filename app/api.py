import string

import flask
import mstranslator
import requests

import config


api = flask.Blueprint('api', __name__)
translator = mstranslator.Translator(config.MS_TRANSLATOR_KEY)


@api.after_request
def set_crossdomain_headers(response):
    """This middleware provides cross-domain requests"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@api.route('/check', methods=['POST'])
def api_check():
    """Computes text stats
    :Errors:
        400: Empty text
    :returns: JSON {
        "total": int,
        "words": int
    }
    """

    try:
        text = flask.request.form['text']
    except KeyError:
        flask.abort(400)
    
    return flask.jsonify({
        'total': sum(symbol.isalpha() or symbol.isdigit() for symbol in text),
        'words': len(text.split()),
    })


@api.route('/translate', methods=['POST'])
def api_translate():
    """Translates the text
    Uses Yandex.Dict to translate a single word
    Uses mstranslate (MS Azure Cognitive Translator) to translate the sentence

    :Errors:
        400: Empty text
        404: Definition not found
        500: Failed to connect to Yandex
    :returns: JSON {
        "text": str,
        "transcription": str or null,
        "translation": str
    }
    """

    try:
        text = flask.request.form['text'].strip()
    except KeyError:
        flask.abort(400)
    if not text:
        flask.abort(400)
    if len(text) > config.MAX_TEXT_LENGTH:
        flask.abort(413)  # Payload Too Large
    

    # If any whitespace symbol is in text then count of words > 1
    if any(symbol in text for symbol in string.whitespace):
        translation = translator.translate(text, lang_to='ru')
        respone = {
            'text': text,
            'translation': translation,
            'transcription': None,
        }
    else:
        yandex_dict_request_data = {
            'text': text,
            'lang': 'en-ru',
            'key': config.YANDEX_DICT_KEY,
        }

        try:
            with requests.post(config.YANDEX_DICT_API_URL, data=yandex_dict_request_data) as res:
                parsed_response = res.json()
        except requests.exceptions.ConnectionError:
            flask.abort(500)

        if not parsed_response['def']:
            flask.abort(404)
        
        definition = parsed_response['def'][0]
        translation_variants = definition['tr'][:config.YANDEX_DICT_MAX_TRANSLATION_VARIANTS]
        respone = {
            'text': definition['text'],
            'transcription': definition.get('ts'),
            'translation': ', '.join(variant['text'] for variant in translation_variants),
        }
        
    return flask.jsonify(respone)
