import flask
import config


views = flask.Blueprint('views', __name__)


@views.route('/')
def route_index():
    return flask.send_from_directory(config.STATIC_FOLDER, 'index.html')
