import flask

import config
import api
import views


app = flask.Flask(__name__, static_folder=config.STATIC_FOLDER)
app.register_blueprint(api.api, url_prefix='/api/')
app.register_blueprint(views.views)
