const SEND_DELAY = 500;


let checkTimeoutId;
input.oninput = () => {
    clearTimeout(checkTimeoutId);
    if (!input.value.trim()) {
        words.innerHTML = total.innerHTML = '';
        return;
    }
    words.innerHTML = total.innerHTML = '';
    spinnerAlphasAndDigits.hidden = spinnerWords.hidden = false;

    const data = new FormData();
    data.set('text', input.value);
    checkTimeoutId = setTimeout(async () => {
        const response = await fetch('/api/check', {
            'method': 'POST',
            'body': data,
        });
        const result = await response.json();
    
        spinnerAlphasAndDigits.hidden = spinnerWords.hidden = true;
        words.innerHTML = result.words;
        total.innerHTML = result.total;
    }, SEND_DELAY);
};


const translatedWords = [];
let translateTimeoutId;
input.onselect = () => {
    clearTimeout(translateTimeoutId);
    const text = getSelectedText();
    if (!text) {
        return;
    }

    if (~translatedWords.indexOf(text)) {
        makeToast('Уже переведено!');
        return;
    }

    const data = new FormData();
    data.set('text', text);
    translateTimeoutId = setTimeout(async () => {
        const response = await fetch('/api/translate', {
            method: 'POST',
            body: data,
        });

        if (getSelectedText() !== text) {
            // Show only if text still selected
            return;
        }

        switch (response.status) {
            case 400:
                makeToast('Неправильный запрос. Наверное.')
                return;
            case 404:
                makeToast('Перевод не найден! А жаль.');
                return;
            case 413:
                makeToast('Текст для перевода должен быть не больше 10000 символов.');
                return;        
            case 500:
                makeToast('Скорее всего, наш сервер не смог подключиться к API Яндекса или MS. Или он просто упал...');
                return;
        }

        const result = await response.json();
        if (result.transcription) {
            translatedWords.push(text);  // Remember word
            outputDict.value += `${result.text} [${result.transcription}] — ${result.translation}\n`;
        } else {
            outputTranslation.value = result.translation;
        }
    }, SEND_DELAY);
};


function makeToast(text) {
    console.log('Toast: ', text);
    toastBody.innerText = text;
    $('#toast').toast('show');
}


function getSelectedText() {
    return getSelection().toString().trim();
}
