# ULSTU Home Reading Helper

## Guide
### Installing requirements
```shell script  
pip install -r requirements.txt  
```  
### Prepare environ 
Set following environment variables:  
```
FLASK_APP=app/main
FLASK_ENV=development
YANDEX_DICT_KEY=
MS_TRANSLATOR_KEY=
```  
You can use `.env` (see https://pypi.org/project/python-dotenv/)

#### Running (On Windows)  
```cmd  
run
```
